import './index.css';
import axios from 'axios';

const getImages = (tags, perPage) => {
    const key = '2e4a3dddc15d38dbd54e2ae566fee7be'
    const url = 'https://www.flickr.com/services/rest/?method=flickr.photos.search'
    return axios.get(url, {
        params: {
            api_key: key,
            tags: tags,
            tag_mode: 'all',
            per_page: perPage,
            extras: 'description, date_taken'
        }
    })
}

const parseXmlString = (xmlString) => {
    const parser = new DOMParser()
    return parser.parseFromString(xmlString, 'application/xml')
}

const imageFactory = (xml) => {
    const images = []
    const xmlImages = xml.getElementsByTagName('photo')
    Array.from(xmlImages).forEach((image) => {
        const newImage = {
            id: '',
            secret: '',
            server: '',
            description: '',
            date_taken: '',
        }
        newImage.id = image.getAttribute('id')
        newImage.secret = image.getAttribute('secret')
        newImage.server = image.getAttribute('server')
        newImage.description = image.getAttribute('title')
        newImage.date_taken = image.getAttribute('datetaken')
        images.push(newImage)
    })
    return images
}

const disciplineImages = document.querySelectorAll('.js-discipline-image');
disciplineImages.forEach((item) => {
    const discipline = item.dataset.discipline

    getImages(discipline, 1)
        .then((response) => {
            const xml = parseXmlString(response.data)
            const image = imageFactory(xml)[0]
            replaceImage(image, item)
        })
        .catch((error) => {
            console.log(error)
        })
})

const replaceImage = (image, item) => {
    if (image) {
        let size = 'w'
        if (item.dataset.size) {
            size = item.dataset.size
        }

        item.setAttribute(
            'src',
            `https://live.staticflickr.com/${image.server}/${image.id}_${image.secret}_${size}.jpg`
        )
    } else {
        loadFallback(item)
    }
    item.classList.add('is-visible')
}

const loadFallback = (image) => {
    const imageFallback = image.dataset.fallback
    image.setAttribute('src', imageFallback)
}

const posts = document.querySelector('.js-posts');
const getPosts = () => {
    const discipline = posts.dataset.discipline
    getImages(discipline, 3)
        .then((response) => {
            const xml = parseXmlString(response.data)
            const posts = imageFactory(xml)
            createPosts(posts)
        })
        .catch((error) => {
            console.log(error)
        })
}

const createPosts = (items) => {
    items.forEach((item) => {
        const post = createPost(item)
        posts.appendChild(post)
    })
}

const createPost = (item) => {
    const formatedDate = new Date(item.date_taken).toLocaleDateString('en-US', {
        month: 'long',
        day: '2-digit',
        year: 'numeric'
    })
    const post = document.createElement('div')
    post.classList.add('post')
    const media = document.createElement('div')
    media.classList.add('post__media')
    const image = document.createElement('img')
    image.classList.add('post__image')
    image.setAttribute('src', `https://live.staticflickr.com/${item.server}/${item.id}_${item.secret}_w.jpg`)
    const content = document.createElement('div')
    content.classList.add('post__content')
    const date = document.createElement('div')
    date.classList.add('post__date')
    date.innerHTML = formatedDate
    const description = document.createElement('p')
    description.innerHTML = item.description

    media.appendChild(image)
    post.appendChild(media)
    content.appendChild(date)
    content.appendChild(description)
    post.appendChild(content)
    return post
}

if (posts) {
    getPosts();
}
